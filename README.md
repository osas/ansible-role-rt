# Ansible role to manage RT installation

## Introduction

[Request Tracker](https://bestpractical.com/request-tracker/) is a ticketing system.

This role installs the website and initializes the database.

Installation of the MySQL database is left to your care and can be remote. We used
the Fedora 25 package, which is made to use MySQL only, so this role currently does
not support using another Database.

You should also polish the website settings, like redirecting the vhost at the /rt
application directory if the vhost is decicated to this service. As it is highly
installation-specific this role does not meddle into this.

Also this role is meant to work on HTTPS only for security reasons, so RT is
configured for port 443.

Opening tickets via mail is supported using fetchmail and IMAP. The role takes care
of installing the needed configuration is the `mail_fetch` structure is defined.

Sending mails requires at least a minimalist MTA list msmtp, but this role does not
install it (see the 'msmtp' or 'postfix' OSAS roles for example).

Example playbook:

```
- hosts: tickets.example.com
  roles:
  - role: mariadb
  - role: rt
    vhost: "{{ ansible_fqdn }}"
    mail_fetch:
      - server: mail.example.com
        user: rt
        passwd: usevaultforsecrets
  - role: httpd
    website_domain: "{{ ansible_fqdn }}"
    redirects:
    - src: '^/$'
      target: '/rt'
      match: True
    use_letsencrypt: True
    force_tls: True
```

## Variables

Main variables:

- **vhost**: web vhost name

Advanced configuration variables:

- **db_server**: database hostname, if not installed locally
- **db_name**: database name, if multiple instances are installed on the same host
- **db_user**: database user, if multiple instances are installed on the same host
- **mail_fetch**: if defined, activates fetching mails via an IMAP server for interacting with tickets
    it is a list of structures, one for each mailbox to poll, with the following fields:
      server: hostname/IP of the IMAP server
      user: login for the IMAP account
      passwd: password for the IMAP account
      queue: in which queue incoming mails should be handled (default to "general" is not defined)
      use_comments: if True, expects a second mailbox to exist with the login being `<user>-comment`
                    and using the same password as `<user>`; it is used to reply to RT comments,
                    which are separate from the ticket correspondance (defaults to True)

